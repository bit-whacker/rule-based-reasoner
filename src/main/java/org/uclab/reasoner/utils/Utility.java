package org.uclab.reasoner.utils;

import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

public class Utility {
  
  private static Logger LOG = LogManager.getRootLogger();
  
  /**
   * Converts stringJson to map and list rules from it.
   * 
   * @param stringRules
   * @return List rules
   */
  public static Map<String, Object> getMap(String json) {
    Map<String, Object> map = new HashMap<String, Object>();
    ObjectMapper mapper = new ObjectMapper();
    try {
      map = mapper.readValue(json, new TypeReference<HashMap<String, Object>>() {});
    } catch (Exception e) {
      e.printStackTrace();
    }
    return map;
  }
  
  /**
   * converts String time to seconds and returns
   * 
   * @param time
   * @return seconds
   */
  public static int timeToSec(String time) {
    time = time.replaceAll(" ", "").toLowerCase();
    if (time.length() < 2) {
      LOG.error("Invalid Time Format: " + time);
      return 0;
    }
    String[] timeParts = time.split(":");
    int seconds = 0;
    for (String part : timeParts) {
      if (part == null || part.length() == 1) {
        continue;
      }
      seconds += getSeconds(part);
    }
    return seconds;
  }
  
  /**
   * converts String part of time to seconds and returns
   * <p>
   * 
   * @param timePart
   * @return seconds
   */
  public static int getSeconds(String timePart) {
    int secs = 0;
    if (timePart.contains("h")) {
      timePart = timePart.replace("h", "");
      if (timePart.length() < 1) {
        LOG.error("Invalid hours: " + timePart);
        return 0;
      }
      int hour = Integer.parseInt(timePart);
      secs = hour * 3600;
    } else if (timePart.contains("m")) {
      timePart = timePart.replace("m", "");
      if (timePart.length() < 1) {
        LOG.error("Invalid minutes: " + timePart);
        return 0;
      }
      int minute = Integer.parseInt(timePart);
      secs = minute * 60;
    } else {
      if (timePart.contains("s")) {
        timePart = timePart.replace("s", "");
        if (timePart.length() < 1) {
          LOG.error("Invalid seconds: " + timePart);
          return 0;
        }
        secs = Integer.parseInt(timePart);
      }
    }
    return secs;
  }
}
