package org.uclab.reasoner.core;

import java.util.List;
import java.util.Map;

import org.uclab.reasoner.model.FiredRule;
import org.uclab.reasoner.model.Symptom;

public abstract class AbstractRecommendationBuilder{
  public abstract List<FiredRule> buildRecommendation(Symptom symptom, List<Map<String, Object>> inputRules);

}
