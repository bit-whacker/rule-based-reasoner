package org.uclab.reasoner.core;

import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.uclab.reasoner.model.FiredRule;
import org.uclab.reasoner.model.Rule;
import org.uclab.reasoner.model.Symptom;

public class RecommendationBuilder extends AbstractRecommendationBuilder {
  private static Logger LOG = LogManager.getRootLogger();
  
  private AbstractPatternMatcher patternMatcher;
  private AbstractConflictResolver conflictResolver;
  
  /**
   * Instantiates RecommendationBuilder by using patternMatcher & conflictResolver
   * 
   * <p>
   * @param patternMatcher
   * @param conflictResolver
   */
  public RecommendationBuilder(AbstractPatternMatcher patternMatcher, AbstractConflictResolver conflictResolver){
    this.patternMatcher = patternMatcher;
    this.conflictResolver = conflictResolver;
  }
  
  /**
   * generates recommendation based on the ISCLDataPacket
   * <p> 
   * 
   * @param ISCLDataPacket
   * @return
   */
  @Override
  public List<FiredRule> buildRecommendation(Symptom symptom, List<Map<String, Object>> inputRules) {
    LOG.debug("building recommendation...");
    Map<String, Object> inputSymptoms = null;
    
    LOG.debug("Firing rules...");
    List<FiredRule> firedRules = patternMatcher.fireRule(inputSymptoms, inputRules);
    LOG.debug("Resolving conflict...");
    List<FiredRule> finalResolvedRules = conflictResolver.resolveConflict(firedRules);
    
    return finalResolvedRules;
  }
}
