package org.uclab.reasoner.core;

public class TypeComparator {
  
  /**
   * Compares v1 and v2 based on the operator defined e.g. op and returns TRUE on success and FALSE on failure
   * <p>
   * 
   * @param v1
   * @param v2
   * @param op
   * @return boolean
   */
  public static boolean compare(String v1, String v2, String op) {
    boolean matched = false;

    if (op.equals("=")) {
      matched = v1.equalsIgnoreCase(v2);
    } else if (op.equals("!=")) {
      matched = !v1.equalsIgnoreCase(v2);
    }

    return matched;
  }
  
  /**
   * Compares v1 and v2 based on the operator defined e.g. op and returns TRUE on success and FALSE on failure
   * <p>
   * 
   * @param v1
   * @param v2
   * @param op
   * @return boolean
   */
  public static boolean compare(int v1, int v2, String op) {
    boolean matched = false;

    switch (op) {

    case "=":
      matched = v1 == v2;
      break;
    case "!=":
      matched = v1 != v2;
      break;
    case "<":
      matched = v1 < v2;
      break;
    case ">":
      matched = v1 > v2;
      break;
    case "<=":
      matched = v1 <= v2;
      break;
    case ">=":
      matched = v1 >= v2;
    }

    return matched;
  }
  
  /**
   * Compares v1 and v2 based on the operator defined e.g. op and returns TRUE on success and FALSE on failure
   * <p>
   * 
   * @param v1
   * @param v2
   * @param op
   * @return boolean
   */
  public static boolean compare(boolean v1, boolean v2, String op) {
    boolean matched = false;

    if (op.equals("=")) {
      matched = v1 == v2;
    } else if (op.equals("!=")) {
      matched = v1 != v2;
    }

    return matched;
  }
}
