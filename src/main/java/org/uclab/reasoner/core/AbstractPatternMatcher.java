package org.uclab.reasoner.core;

import java.util.List;
import java.util.Map;

import org.uclab.reasoner.model.FiredRule;

public abstract class AbstractPatternMatcher {
  
  /**
   * matches rules/conditions against user data and returns list of all applicable rules 
   * <p>
   * 
   * @param conditionsValue
   * @param rules
   * @return List Of FiredRule
   */
  public abstract List<FiredRule> fireRule(Map<String, Object> conditionsValue, List<Map<String, Object>> rules);
}
