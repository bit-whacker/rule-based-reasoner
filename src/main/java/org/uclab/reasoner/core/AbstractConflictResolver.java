package org.uclab.reasoner.core;

import java.util.List;

import org.uclab.reasoner.model.FiredRule;

public abstract class AbstractConflictResolver {
  
  /**
   * resolve's conflict by selecting the most appropriate rule from the set of all applicable rules
   * 
   * @param firedRules
   * @return List FiredRules
   */
  public abstract List<FiredRule> resolveConflict(List<FiredRule> firedRules);
}
