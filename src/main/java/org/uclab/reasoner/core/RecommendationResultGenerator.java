package org.uclab.reasoner.core;

import java.io.IOException;
import java.util.List;

import org.codehaus.jackson.map.ObjectMapper;
import org.uclab.reasoner.model.FiredRule;

public class RecommendationResultGenerator {

  private List<FiredRule> firedRules;
  
  /**
   * instantiates Recommendation Result Generator
   * 
   */
  public RecommendationResultGenerator() {
  }
  
  /**
   * instantiates Recommendation Result Generator
   * 
   * @param firedRules
   */
  public RecommendationResultGenerator(List<FiredRule> firedRules) {
    this.firedRules = firedRules;
  }
  
  /**
   * builds complex json object
   * <p>
   * 
   * @return JSON
   */
  public String generateResults() {
    String jsonResult = null;
    try {
      jsonResult = new ObjectMapper().writeValueAsString(firedRules);
    } catch (IOException ex) {
      ex.printStackTrace();
    }
    return jsonResult;
  }
}
