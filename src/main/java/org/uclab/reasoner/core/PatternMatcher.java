package org.uclab.reasoner.core;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.uclab.reasoner.model.Conclusion;
import org.uclab.reasoner.model.FiredRule;
import org.uclab.reasoner.utils.Utility;

public class PatternMatcher extends AbstractPatternMatcher{
  private static Logger LOG = LogManager.getRootLogger();
  
  /**
   * fires rule for a specific situation after reasoning
   * <p>
   * 
   * @param conditionsValue
   * @param rules
   * @return listOfMatchedRules
   */
  @SuppressWarnings("unchecked")
  @Override
  public List<FiredRule> fireRule(Map<String, Object> conditionsValue, List<Map<String, Object>> rules) {
    List<FiredRule> firedRules = new ArrayList<FiredRule>();

    for (Map<String, Object> rule : rules) {
      List<Map<String, Object>> conditionsKeyList = (List<Map<String, Object>>) rule.get("conditionList");
      if (matchRule(conditionsValue, conditionsKeyList)) {
        List<Map<String, Object>> conclusionsList = (ArrayList<Map<String, Object>>) rule.get("conclusionList");
        int numberOfConditions = ((List<Map<String, Object>>) rule.get("conditionList")).size();
        FiredRule firedRule = new FiredRule(rule.get("ruleID").toString());
        firedRule.setRuleConclusion(rule.get("ruleConclusion").toString());
        firedRule.setConditionsKey(numberOfConditions);
        for (Map<String, Object> conclusionMap : conclusionsList) {
          Conclusion conclusion = new Conclusion(conclusionMap.get("conclusionKey").toString());
          conclusion.setConclusionValue(conclusionMap.get("conclusionValue").toString());
          conclusion.setConclusionOperator(conclusionMap.get("conclusionOperator").toString());
          firedRule.addConclusion(conclusion);
        }
        firedRules.add(firedRule);
      }
    }
    return firedRules;
  }
  
  /**
   * returns true if all conditions matched false other wise
   * 
   * @param conditionsValue
   * @param conditionsKeyList
   * @return boolean
   */
  private boolean matchRule(Map<String, Object> conditionsValue, List<Map<String, Object>> conditionsKeyList) {
    boolean CONDITION_MATCHED = false;
    for (Map<String, Object> conditionsKey : conditionsKeyList) {
      CONDITION_MATCHED = false;
      for (String FactKey : conditionsValue.keySet()) {
        if (matchCondition(FactKey, conditionsValue.get(FactKey), conditionsKey)) {
          CONDITION_MATCHED = true;
          break;
        }
      }
      if (!CONDITION_MATCHED) {
        return false;
      }
    }
    return true;
  }

  /**
   * returns TRUE if FactValue and RuleValue matched according to the operator
   * defined in the rule, FALSE elsewhere.
   * 
   * @param factKey
   * @param factValue
   * @param condition
   * @return boolean
   */
  private boolean matchCondition(String factKey, Object factValue, Map<String, Object> condition) {
    Boolean matched = false;
    try {
      if(!factKey.equalsIgnoreCase(condition.get("conditionKey").toString()))
        return false;
      
      String conditionType = null;
      try {
        conditionType = condition.get("conditionType").toString();
      } catch (Exception ex) {
        LOG.error("ConditionType is null! Setting to 'String'");
        conditionType = "String";
      }
      
      switch (conditionType) {
      case "string":
      case "String":
        matched = TypeComparator.compare(factValue.toString(), condition.get("conditionValue").toString(), condition.get("conditionValueOperator").toString());
      break;
      case "Integer":
      case "integer":
      case "Int":
      case "int":
        matched = TypeComparator.compare(Integer.parseInt(factValue.toString()), Integer.parseInt(condition.get("conditionValue").toString()), condition.get("conditionValueOperator").toString());
      break;
      case "boolean":
      case "Boolean":
        matched = TypeComparator.compare((boolean) factValue.toString().equalsIgnoreCase("yes"), (boolean) condition.get("conditionValue").toString().equalsIgnoreCase("yes"), condition.get("conditionValueOperator").toString());
      break;
      case "Time":
      case "time":
        int factTimeSec = Utility.timeToSec(factValue.toString());
        int ruleTimeSec = Utility.timeToSec(condition.get("conditionValue").toString());
        matched = TypeComparator.compare(factTimeSec, ruleTimeSec, condition.get("conditionValueOperator").toString());
      break;
      default:
        LOG.error("Type: " + condition.get("conditionType") + " is not defined!");
      }
    } catch (Exception ex) {
      matched = false;
      LOG.error("Invalid type conversion " + ex.getMessage());
    }

    return matched;
  }
}
