package org.uclab.reasoner.core;

import java.util.ArrayList;
import java.util.List;

import org.uclab.reasoner.model.FiredRule;

public class ConflictResolver extends AbstractConflictResolver{
  
  /**
   * Resolves conflict by using maximum specificity technique and returns the final resolved rule/s
   * 
   * @param firedRules
   * @return finalResolvedRules
   */
  @Override
  public List<FiredRule> resolveConflict(List<FiredRule> firedRules) {
    if (firedRules == null || firedRules.size() < 2) {
      return firedRules;
    }
    List<FiredRule> resolvedRules = new ArrayList<FiredRule>();
    int max = 0;
    for (FiredRule rule : firedRules) {
      int numConditions = rule.getConditionsKey();

      if (max == numConditions) {
        resolvedRules.add(rule);
      } else if (numConditions > max) {
        resolvedRules.clear();
        resolvedRules.add(rule);
        max = numConditions;
      }
    }
    return resolvedRules;
  }
}
