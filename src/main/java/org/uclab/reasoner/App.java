package org.uclab.reasoner;

import java.util.List;
import java.util.Map;

import org.uclab.reasoner.core.ConflictResolver;
import org.uclab.reasoner.core.PatternMatcher;
import org.uclab.reasoner.core.RecommendationBuilder;
import org.uclab.reasoner.model.FiredRule;
import org.uclab.reasoner.model.Situations;
import org.uclab.reasoner.model.Symptom;
import org.uclab.reasoner.service.RulesService;

public class App 
{
    public static void main( String[] args ){

      RulesService rulesService = new RulesService();
      Symptom symptom = null;
      
      List<Map<String, Object>> rules = rulesService.setSituationsData(new Situations()).connect().getResponse().parse();
      
      RecommendationBuilder r = new RecommendationBuilder(new PatternMatcher(), new ConflictResolver());
      List<FiredRule> finalResolvedRules = r.buildRecommendation(symptom, rules);
    }
}
