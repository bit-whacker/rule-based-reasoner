package org.uclab.reasoner.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class FiredRule implements Serializable {
  /**
	 * 
	 */
  private static final long serialVersionUID = -6568646491998798139L;

  private String ruleId;
  private String ruleConclusion;
  private List<Conclusion> conclusionList = new ArrayList<Conclusion>();
  private int conditionsKey = 0;

  public FiredRule() {
  }

  public FiredRule(String ruleId) {
    this.ruleId = ruleId;
  }

  public String getRuleId() {
    return ruleId;
  }

  public void setRuleId(String ruleId) {
    this.ruleId = ruleId;
  }

  public List<Conclusion> getConclusionList() {
    return conclusionList;
  }

  public void setConclusionList(List<Conclusion> conclusionList) {
    this.conclusionList = conclusionList;
  }

  public void addConclusion(Conclusion conclusion) {
    conclusionList.add(conclusion);
  }

  public String getRuleConclusion() {
    return ruleConclusion;
  }

  public void setRuleConclusion(String ruleConclusion) {
    this.ruleConclusion = ruleConclusion;
  }

  public int getConditionsKey() {
    return conditionsKey;
  }

  public void setConditionsKey(int conditionsKey) {
    this.conditionsKey = conditionsKey;
  }
}
