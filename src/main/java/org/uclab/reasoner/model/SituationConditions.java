/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.uclab.reasoner.model;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author
 */
@XmlRootElement()
public class SituationConditions implements Serializable {

  private String conditionKey;
  private String conditionValueOperator;
  private String conditionValue;
  private String conditionType;

  public SituationConditions() {

  }

  /**
   * SituationCondions
   * 
   * @param conditionKey
   * @param conditionValue
   * @param conditionType
   * @param conditionValueOperator
   */
  public SituationConditions(String conditionKey, String conditionValue,
      String conditionType, String conditionValueOperator) {
    this.conditionKey = conditionKey;
    this.conditionValue = conditionValue;
    this.conditionType = conditionType;
    this.conditionValueOperator = conditionValueOperator;
  }

  /**
   * @return the conditionKey
   */
  public String getConditionKey() {
    return conditionKey;
  }

  /**
   * @param conditionKey
   *          the conditionKey to set
   */
  public void setConditionKey(String conditionKey) {
    this.conditionKey = conditionKey;
  }

  /**
   * @return the conditionValueOperator
   */
  public String getConditionValueOperator() {
    return conditionValueOperator;
  }

  /**
   * @param ConditionValueOperator
   *          the conditionValueOperator to set
   */
  public void setConditionValueOperator(String ConditionValueOperator) {
    this.conditionValueOperator = ConditionValueOperator;
  }

  /**
   * @return the conditionValue
   */
  public String getConditionValue() {
    return conditionValue;
  }

  /**
   * @param ConditionValue
   *          the conditionValue to set
   */
  public void setConditionValue(String ConditionValue) {
    this.conditionValue = ConditionValue;
  }

  /**
   * @return the conditionType
   */
  public String getConditionType() {
    return conditionType;
  }

  /**
   * @param ConditionType
   *          the conditionType to set
   */
  public void setConditionType(String ConditionType) {
    this.conditionType = ConditionType;
  }

  @Override
  public String toString() {
    String keyValue = "\"" + conditionKey + "\":\"" + conditionValue + "\"";

    return keyValue;
  }

}
