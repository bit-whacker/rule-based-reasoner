package org.uclab.reasoner.model;

public class Conclusion {
  String conclusionKey;
  String conclusionValue;
  String conclusionOperator;

  public Conclusion() {
  }

  public Conclusion(String conclusionKey) {
    this.conclusionKey = conclusionKey;
  }

  public String getConclusionKey() {
    return conclusionKey;
  }

  public void setConclusionKey(String conclusionKey) {
    this.conclusionKey = conclusionKey;
  }

  public String getConclusionValue() {
    return conclusionValue;
  }

  public void setConclusionValue(String conclusionValue) {
    this.conclusionValue = conclusionValue;
  }

  public String getConclusionOperator() {
    return conclusionOperator;
  }

  public void setConclusionOperator(String conclusionOperator) {
    this.conclusionOperator = conclusionOperator;
  }

}
